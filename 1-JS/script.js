'used strict';


let randomNumber = Math.floor(Math.random() * 100) + 1;
const guesses = document.querySelector('.guesses');
const lastResult = document.querySelector('.lastResult');
const lowOrHigh = document.querySelector('.lowOrHigh');
const guessSubmit = document.querySelector('.guessSubmit');
const guessField = document.querySelector('.guessField');
let guessCount = 1;
let resetButton;







function checkGuess() {
  const userGuess = Number(guessField.value);
  
  if (userGuess === randomNumber) {
    lastResult.textContent = 'Felicidades has acertado!!!!';
    lowOrHigh.textContent = '';
    setGameOver();
  } else if (guessCount === 5) {
    lastResult.textContent = '!!!Has Perdido!!!';
    lowOrHigh.textContent = '';
    setGameOver();
  } else {
    lastResult.textContent = 'Te has equivocado!!!!';
    if(userGuess < randomNumber) {
      lowOrHigh.textContent = 'Ese numero es muy bajo!' ;
    } else if(userGuess > randomNumber) {
      lowOrHigh.textContent = 'Ese numero es muy alto!';
    }
  }

  guessCount++;
  guessField.value = '';
  guessField.focus();
}






guessSubmit.addEventListener('click', checkGuess);




function resetGame() {
  guessCount = 1;
  const resetP = document.querySelectorAll('.resultP p');
  for (const resetP of resetP) {
    resetP.textContent = '';
  }

  resetButton.parentNode.removeChild(resetButton);
  guessField.disabled = false;
  guessSubmit.disabled = false;
  guessField.value = '';
  guessField.focus();
  lastResult.style.backgroundColor = 'white';
  randomNumber = Math.floor(Math.random() * 100) + 1;
}

